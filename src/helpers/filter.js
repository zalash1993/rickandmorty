const statusFil = (status) => {
    const newStatus = {
        alive:"живой",
        dead:"мертвый",
        unknown:"неизвестно"
    }
    const result = newStatus[status?.toLowerCase()] || status?.toLowerCase();
return  result;
}
const genderFil = (gender) => {
    const newStatus = {
        female:"женщина",
        male:"мужчина",
        genderless:"бесполый",
        unknown:"неизвестно"
    }
    const result = newStatus[gender?.toLowerCase()] || gender?.toLowerCase();
return  result;
}
const speciesFil = (species) => {
    const newStatus = {
        human:"человек",
        alien:"инопланетянин",
        humanoid:"гуманоид",
        unknown:"неизвестно"
    }
    const result = newStatus[species?.toLowerCase()] || species?.toLowerCase();
return result;
}
export {
    statusFil,
    genderFil,
    speciesFil
};