const ITEM_WIDTH = 400;
const generateIndexesForRow = (rowIndex, maxItemsPerRow, itemsAmount) => {
    const result = [];
    const startIndex = rowIndex * maxItemsPerRow;
    for (let i = startIndex; i < Math.min(startIndex + maxItemsPerRow, itemsAmount); i++) {
        result.push(i);
    }
    return result;
}
const getMaxItemsAmountPerRow = (width) => {
    return Math.max(Math.floor(width / ITEM_WIDTH), 1);
}
const getRowsAmount = (width, itemsAmount, hasMore) => {
    const maxItemsPerRow = getMaxItemsAmountPerRow(width);
    return Math.ceil(itemsAmount / maxItemsPerRow) + (hasMore ? 1 : 0);
}
export {
    generateIndexesForRow,
    getMaxItemsAmountPerRow,
    getRowsAmount
};