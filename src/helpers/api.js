import axios from 'axios';

class Api {
    constructor() {
        // configuration
        this.api = axios.create({
            baseURL: "https://rickandmortyapi.com/api/",
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
        });
    }

    listener() {
        const token = localStorage.getItem('token');
        if (token && this.api.defaults.headers.common.Authorization !== `Bearer ${token}`) {
            this.api.defaults.headers.common.Authorization = `Bearer ${token}`;
        }
    }

    setStore(store) {
        this.store = store;
        this.store.subscribe(this.listener.bind(this));
    }
}

export default Api;