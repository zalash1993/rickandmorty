import { applyMiddleware, createStore } from 'redux';
import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './reducers';
import sagas from './sagas'

export default (extra) => {
  const middlewares = createSagaMiddleware();
  let store;
  store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(middlewares)),
  );
  middlewares.run(sagas, extra)
  return store;
};
