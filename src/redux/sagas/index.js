
import { all } from 'redux-saga/effects';
import characters from './characters';
import characterCurrent from './characterCurrent';

export default function* rootSaga(api) {
  yield all ([
    characters(api),
    characterCurrent(api)
  ])
};
