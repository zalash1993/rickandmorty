import { put, call, takeEvery, select } from 'redux-saga/effects'
import { toast } from 'react-toastify';
import charactersAction from '../actions/charactersAction';
import types from '../Types';

function* characterCurrentRequset({id}, { api, audio }) {
  try {
    const { isRequested } = yield select((state) => state.characters);
    if (isRequested) {
      return
    }
    yield put(charactersAction.charactersRequest());

      audio.muted = false; 
      yield audio.play().catch(el => {
        console.log('oyyy')
      })
      const {data} = yield call(api, {
        method: 'GET',
        url: `/character/${id}`
      });
      yield put(charactersAction.characterCurrent(data));
    }
  catch ({ response }) {
    const error = response?.data.error || 'oyy';
    yield put(charactersAction.charactersError(error));
    toast.error('Что-то пошло не так');
  }
}
function* characterCurrent(api) {
  yield takeEvery(types.CURRENT_CHARACTER_REQUEST, (el) => characterCurrentRequset({ ...el }, api));
}


export default characterCurrent;
