import { put, call, takeEvery, select } from 'redux-saga/effects'
import { toast } from 'react-toastify';
import charactersAction from '../actions/charactersAction';

function* charactersRequset(page, { api, audio }) {
  try {
    const charactersArr = yield select((state) => state.characters.characters);
    const { currentPage, info: { pages }, isRequested } = yield select((state) => state.characters);
    if (isRequested) {
      return
    }
    yield put(charactersAction.charactersRequest());

    if (currentPage > pages) {
      yield put(charactersAction.charactersStop());
      toast.success('Все персонажи загружены');
    } else {
      audio.muted = false; 
      yield audio.play().catch(el => {
        console.log('oyyy')
      })
      const { data: { results, info } } = yield call(api, {
        method: 'GET',
        url: `/character/`,
        params: {
          page: currentPage
        }
      });
      yield put(charactersAction.characters(results, charactersArr, info));
    }
  }
  catch ({ response }) {
    const error = response?.data.error || 'oyy';
    yield put(charactersAction.charactersError(error));
    toast.error('Что-то пошло не так');
  }
}
function* characters(api) {
  yield takeEvery("USER_FETCH_REQUESTED", (el) => charactersRequset({ ...el }, api));
}


export default characters;
