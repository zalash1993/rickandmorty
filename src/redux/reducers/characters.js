import types from '../Types'

const initialvalues = {
  isRequested: false,
  characters: [],
  characterCur:{},
  info: {
    pages: 33
  },
  currentPage:1,
  requestedBool:true,
  error: '',
};

const characters = (state = initialvalues, action) => {
  switch (action.type) {
    case types.CHARACTERS_REQUEST:
      return { ...state, isRequested: true, error: '' };
    case types.CHARACTERS_REQUEST_SUCCESS:
      return {
        ...state,
        isRequested: false,
        characters: action.characters,
        info: action.info,
        currentPage: state.currentPage+1,
        error: ''
      };
      case types.CHARACTERS_REQUEST_ERROR:
        return {
          ...state,
          isRequested: false,
          error: action.error,
        };
        case types.CURRENT_CHARACTER_REQUEST_SUCCESS:
          return {
            ...state,
            isRequested: false,
            error: '',
            characterCur:action.characterCur
          };
        case types.CHARACTERS_REQUEST_STOP:
          return {
            ...state,
            isRequested: false,
            requestedBool:false,
            error:'',
          };
    ////////////
    default:
      return state;
  }
}

export default characters;
