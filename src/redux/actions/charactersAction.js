import types from '../Types';

const charactersRequest = () => ({ type: types.CHARACTERS_REQUEST });
const charactersStop = () => ({ type: types.CHARACTERS_REQUEST_STOP });
const characters = (array, preArr, info) => {
  const newArr = [...preArr, ...array];
  return ({ type: types.CHARACTERS_REQUEST_SUCCESS, characters: newArr, info });
}
const characterCurrent = (obj) => {
  return ({ type: types.CURRENT_CHARACTER_REQUEST_SUCCESS, characterCur: obj });
}
const charactersError = (error) => ({ type: types.CHARACTERS_REQUEST_ERROR, error });
const characterCurrentRequest = (id) => ({ type: types.CURRENT_CHARACTER_REQUEST, id });
export default {
  characters,
  charactersError,
  charactersRequest,
  charactersStop,
  characterCurrent,
  characterCurrentRequest
};
