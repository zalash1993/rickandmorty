import React, { useEffect } from 'react';
import cls from './App.module.scss';
import { Route, Switch } from 'react-router-dom';
import Home from './pages/Home';
import CurrentCharacter from './pages/CurrentCharacter';
import Page404 from './pages/Page404';
import Header from './components/Header';
import { useDispatch } from 'react-redux';

function App() {
  const dispatch = useDispatch();
    useEffect(() => {
        dispatch({ type: 'USER_FETCH_REQUESTED' })
    }, [dispatch]);
  return (
    <div className={cls.App}>
      <Header />
      <div className={cls.home_content}>
        <Switch>
          <Route exact path='/' component={Home} />
          <Route exact path='/current-character/:id' component={CurrentCharacter} />
          <Route component={Page404} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
