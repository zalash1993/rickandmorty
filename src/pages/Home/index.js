import React, { Fragment } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import CharactersBox from '../../components/CharactersBox';
import Spinner from '../../components/Spinner';
import cls from './home.module.scss';
import { FixedSizeList as List } from 'react-window';
import InfiniteLoader from 'react-window-infinite-loader';
import { generateIndexesForRow, getMaxItemsAmountPerRow, getRowsAmount } from '../../helpers/windowHelper';
import HelmetComponent from '../../components/HelmetComponent';

const ITEM_HEIGHT = 395;

const Home = () => {
    const dispatch = useDispatch();
    const rows = useSelector(state => state.characters.characters);
    const { isRequested, requestedBool } = useSelector(state => state.characters);
    /////////////////////////
    const width = window.innerWidth;
    const height = window.innerHeight - 130;
    /////////////////////////
    const rowRenderer = ({ index, style }) => {
        const maxItemsPerRow = getMaxItemsAmountPerRow(width);
        const moviesIds = generateIndexesForRow(index, maxItemsPerRow, rows.length).map(movieIndex => rows[movieIndex]);
        return (
            <div style={style} className={cls.row_box}>
                {moviesIds.map(movieId => {
                    return (
                        <Fragment key={movieId.id}>
                            <CharactersBox key={movieId.id} {...movieId} />
                        </Fragment>
                    )
                })}
            </div>
        )
    };
    const rowCount = getRowsAmount(width, rows.length, requestedBool);
    return (
        <div className={cls.home_container}>
            <HelmetComponent titlePage="Rick and Morty" description='Rick и Morty спешат на помощь' title="Rick and Morty" />
            <Spinner show={isRequested} fullScreen />
            <InfiniteLoader
                itemCount={rowCount}
                isItemLoaded={(index) => {
                    const maxItemsPerRow = getMaxItemsAmountPerRow(width);
                    const allItemsLoaded = generateIndexesForRow(index, maxItemsPerRow, rows.length).length > 0;
                    return !requestedBool || allItemsLoaded;
                }}
                loadMoreItems={() => {
                    dispatch({ type: 'USER_FETCH_REQUESTED' })
                }}
            >
                {({ onItemsRendered, ref }) => {
                    return (
                        <section>
                            <List
                                ref={ref}
                                height={height}
                                width={width}
                                itemCount={rowCount}
                                itemSize={ITEM_HEIGHT}
                                onItemsRendered={onItemsRendered}
                            >
                                {rowRenderer}
                            </List>
                        </section>
                    )
                }}
            </InfiniteLoader>
        </div>
    )
}

export default Home;