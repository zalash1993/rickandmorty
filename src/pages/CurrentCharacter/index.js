import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Spinner from '../../components/Spinner';
import charactersAction from '../../redux/actions/charactersAction';
import cls from './currentCharacter.module.scss';
import preload from '../../img/preload.png';
import { statusFil,genderFil,speciesFil } from '../../helpers/filter';
import HelmetComponent from '../../components/HelmetComponent';

const CurrentCharacter = ({ match: { params: { id } } }) => {
    const dispatch = useDispatch();
    const { isRequested, characterCur: { image, name, gender, status, created, species } } = useSelector(state => state.characters);
    useEffect(() => {
        dispatch(charactersAction.characterCurrentRequest(id))
    }, [dispatch, id]);
    return (<div className={cls.curr_wrapper}>
        <HelmetComponent titlePage={name} description='Rick и Morty спешат на помощь' title={name} />
        <Spinner show={isRequested} fullScreen />
        <h1>{name}</h1>
        <div className={cls.curr_container}>
            <div className={cls.box_img}>
                <img
                    className={cls.img_preload}
                    loading="lazy"
                    src={preload}
                    alt="preload"
                />
                <img
                    className={cls.img_load}
                    loading="lazy"
                    src={image}
                    alt={name}
                />
            </div>
            <div className={cls.block_info}>
                <div className={cls.cur_info}>
                    <div className={cls.cur_info_first}>Статус</div>
                    <div className={cls.cur_info_second}>{statusFil(status)}</div>
                </div>
                <div className={cls.cur_info}>
                    <div className={cls.cur_info_first}>Пол</div>
                    <div className={cls.cur_info_second}>{genderFil(gender)}</div>
                </div>
                <div className={cls.cur_info}>
                    <div className={cls.cur_info_first}>Вид</div>
                    <div className={cls.cur_info_second}>{speciesFil(species)}</div>
                </div>
                <div className={cls.cur_info}>
                    <div className={cls.cur_info_first}>Создан</div>
                    <div className={cls.cur_info_second}>{new Date(created)?.toLocaleDateString("ru-RU")}</div>
                </div>
            </div>
        </div>
    </div>)
}

export default CurrentCharacter;