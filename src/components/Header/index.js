import React from 'react';
import cls from './header.module.scss';
import logo from '../../img/logo.png';
import { Link } from 'react-router-dom';
import json from '../../../package.json';

const Header = () => {
    return (
        <header className={cls.header_container}>
            <div className={cls.header_wrapper}>
                <div className={cls.version}>v. {json.version}</div>
                <Link to="/" className={cls.header_img}>
                    <img src={logo} alt="logo" />
                </Link>
            </div>
        </header>
    )
}

export default Header;
