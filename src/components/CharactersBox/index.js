import React from 'react';
import { Link } from 'react-router-dom';
import cls from './charactersBox.module.scss';
import preload from '../../img/preload.png';

const CharactersBox = ({ id, name, image}) => {
    return (
        <div className={cls.charactersBox_wrapper} key={id}
        >
            <Link to={`/current-character/${id}`}
            >
                <h1>{name}</h1>
                <div>
                    <div className={`${cls.box_img} ${true ? cls.silver_img : ""}`}>
                        <img
                            className={cls.img_preload}
                            loading="lazy"
                            src={preload}
                            alt="preload"
                        />
                        <img
                            className={cls.img_load}
                            loading="lazy"
                            src={image}
                            alt={name}
                        />
                        <div className={cls.silver_block}>
                            <span>подробнее</span>
                        </div>
                    </div>
                </div>
            </Link>
        </div>
    )
}

export default CharactersBox;
