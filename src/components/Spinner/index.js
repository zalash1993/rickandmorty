import React from 'react';
import PropTypes from 'prop-types';
import './style.scss';
import spinner from '../../img/loading.gif';

const Spinner = ({ show, fullScreen, isNoBackground, onPlay }) => {
  onPlay(show);
  if (!show) {
    document.body.style.overflowY = 'auto';
    return '';
  }
  document.body.style.overflowY = 'hidden';
  let style = fullScreen ? 'spinner-bgFull' : 'spinner-bg';
  if (isNoBackground) {
    style = `${style}-not-bg`;
  }
  return (
    <div className={style}>
      <img src={spinner} alt='spinner' />
    </div>
  );
};

Spinner.propTypes = {
  show: PropTypes.bool.isRequired,
  isNoBackground: PropTypes.bool,
  fullScreen: PropTypes.bool,
};

Spinner.defaultProps = {
  fullScreen: false,
  isNoBackground: false,
  onPlay:()=>{}
};

export default Spinner;
