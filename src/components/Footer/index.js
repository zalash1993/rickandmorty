import React from 'react';
import cls from './footer.module.scss';
import json from '../../../package.json';

const Footer = () => {
    return (
        <footer className={cls.footer_container}>
            <div>
            v. {json.version}
        </div>
        </footer>
    )
}


export default Footer;
