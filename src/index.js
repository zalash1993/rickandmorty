import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Api from './helpers/api';
import Store from './redux/Store';
import { HelmetProvider } from 'react-helmet-async';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import sound from './sound/Loading1.mp3';

/// sound
let audio = new Audio(sound);
const play = (el) =>{
  audio.muted = true;
  audio.play();
  document.removeEventListener(el.type,play);
}
document.addEventListener("mousemove", play);
document.addEventListener("click", play);
document.addEventListener("touchend", play);
document.addEventListener("touchmove", play);
///

const api = new Api();
const store = Store({
  api: api.api, audio
});
api.setStore(store);
const rootElement = document.getElementById('root');
let method = 'render';
ReactDOM[method](
  <Provider store={store}>
    <BrowserRouter basename={process.env.PUBLIC_URL}>
      <HelmetProvider >
        <ToastContainer />
        <App />
      </HelmetProvider>
    </BrowserRouter>
  </Provider>,
  rootElement,
);
serviceWorker.unregister();